extends KinematicBody2D
class_name Bullet


export(float) var bullet_speed: float = 750
export(float) var bullet_max_travel: float = 1024
var _initial_pos: Vector2

var current_linear_velocity: Vector2;


func _ready() -> void:
	_initial_pos = position

func _physics_process(delta: float) -> void:
	current_linear_velocity = move_and_slide(get_linear_velocity())
	# print(current_linear_velocity)
	if position.distance_to(_initial_pos) > bullet_max_travel:
		_destroy_bullet()
	if current_linear_velocity.length() < 0.8 * bullet_speed:
		_destroy_bullet()
	return


func get_linear_velocity() -> Vector2:
	if has_node("CurvableBulletPerk"):
		var new_lin_v = current_linear_velocity + $CurvableBulletPerk.get_deviation_vector()
		# print (new_lin_v, new_lin_v.normalized())
		new_lin_v = new_lin_v.normalized() * bullet_speed
		return new_lin_v
	return current_linear_velocity

func _destroy_bullet() -> void:
	current_linear_velocity = Vector2(0,0)
	# get_node("Sprite").visible = false
	var explosion_scene: = load("res://src/objects/bullets/CircleExplosion.tscn")
	var explosion: CircleExplosion = explosion_scene.instance()
	get_tree().current_scene.add_child(explosion)
	explosion.play_explosion_at(self.position)
	queue_free()
	
	
	
	
	
func set_bullet_params(direction: bool) -> void:
	current_linear_velocity = Vector2(bullet_speed * (1.0 if direction else -1.0), 0)


func _on_BulletArea2D_body_entered(body: Node) -> void:
	print ("CULO ", body.get_groups())
	if body.is_in_group("enemies") || body.is_in_group("world"):
		_destroy_bullet()
