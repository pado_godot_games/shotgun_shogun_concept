extends Area2D

class_name CircleExplosion

func play_explosion_at(position: Vector2) -> void:
	self.position = position
	$AnimatedSprite.play("explode")
	yield($AnimatedSprite, "animation_finished")
	$AnimatedSprite.play("fade")
	$ExplosionCollision.queue_free()
	yield($AnimatedSprite, "animation_finished")
	queue_free()
	return
