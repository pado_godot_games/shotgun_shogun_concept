extends Node

export var up_force: float = 15
export var down_force: float = 15
export var left_force: float = 15
export var right_force: float = 15

func get_deviation_vector() -> Vector2:
	return Vector2(
		right_force * Input.get_action_strength("action_right") - left_force * Input.get_action_strength("action_left"),
		down_force * Input.get_action_strength("action_down") - up_force * Input.get_action_strength("action_up")
	)
	
