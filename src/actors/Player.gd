extends Actor
class_name Player


export var stomp_impulse: float = 1000.0


# true = forward (right)
# false = back (left) 
onready var point_direction: bool = true


func _ready() -> void:
	get_node("AnimatedSprite").play("idle")
	
func _input(event: InputEvent) -> void:
	if event.is_action_pressed("shoot"):
		# get_node("AnimatedSprite").stop()
		if $ShootPerk.explode_bullet():
			$AnimatedSprite.play("fuckyou")


func doShoot(bullet_index: int = 0) -> void:
	var didShot: bool = $ShootPerk.shot(point_direction, bullet_index)
	get_node("AnimatedSprite").play("attack") if didShot else get_node("AnimatedSprite").play("fuckyou")

func _physics_process(delta: float) -> void:
	var is_jump_interrupted: bool = Input.is_action_just_released("jump") and _velocity.y < 0.0
	var direction: = get_direction()
	_velocity = calculate_move_velocity(_velocity, direction, speed, is_jump_interrupted)
	_velocity = move_and_slide(_velocity, FLOOR_NORMAL)
	
	if (position.y > 1000):
		position = Vector2(200,700)
	return
	
func _process(delta: float) -> void:
	var hand_sign_direction: Vector2 = Vector2(
		Input.get_action_strength("action_right") - Input.get_action_strength("action_left"),
		Input.get_action_strength("action_down") - Input.get_action_strength("action_up")
	)
	#if hand_sign_direction.length_squared() == 0: $AnimatedSprite.play("idle")
	if hand_sign_direction.length_squared() == 0: pass
	elif hand_sign_direction.angle_to(Vector2(1,1)) < PI/8: $AnimatedSprite.play("handsForwardDown_idle")
	elif hand_sign_direction.angle_to(Vector2(1,0)) < PI/8: $AnimatedSprite.play("handsForward_idle")
	elif hand_sign_direction.angle_to(Vector2(1,-1)) < PI/8: $AnimatedSprite.play("handsForwardUp_idle")
	#elif hand_sign_direction == Vector2(1,1): $AnimatedSprite.play("handsForwardDown_idle")
	#elif hand_sign_direction == Vector2(1,0): $AnimatedSprite.play("handsForward_idle")
	#elif hand_sign_direction == Vector2(1,-1): $AnimatedSprite.play("handsForwardUp_idle")
	# if Input.get_action_strength("action_right") + Input.get_action_strength("action_left") + Input.get_action_strength("action_down") + Input.get_action_strength("action_up") == 0:
	#	$AnimatedSprite.play("idle")




func get_direction() -> Vector2:
	var leftMovement: float = -1 * Input.get_action_strength("move_left")
	var rightMovement: float = Input.get_action_strength("move_right")
	
	# 0 nothing pressed
	# positive = right
	# negative = left
	var newDirection: float = leftMovement + rightMovement;
	var old_point_direction: bool = point_direction
	if newDirection > 0:
		point_direction = true
	elif newDirection < 0:
		point_direction = false
	flip_player_if_necessary(old_point_direction, point_direction)

	
	return Vector2(
		leftMovement + rightMovement,
		-1.0 if Input.is_action_just_pressed("jump") and is_on_floor() else 1.0
		# -1.0 if Input.is_action_just_pressed("jump") else 1.0
	)
	
	
func flip_player_if_necessary(old_dir: bool, dir: bool) -> void:
	if (old_dir != dir):
		if dir:
			$AnimationPlayer.play("flip_right")
		else:
			$AnimationPlayer.play("flip_left")
	

	
func calculate_move_velocity(
		linear_velocity: Vector2, 
		direction: Vector2,
		speed: Vector2,
		is_jump_interrupted: bool
	) -> Vector2:
		var new_velocity: = linear_velocity
		new_velocity.x = speed.x * direction.x
		new_velocity.y += gravity * get_physics_process_delta_time()
		if direction.y == -1.0:
			new_velocity.y = speed.y * direction.y
		if is_jump_interrupted:
			new_velocity.y = 0.0;
		return new_velocity

func calculate_stomp_velocity(linear_velocity: Vector2, impulse: float) -> Vector2:
	var aux: Vector2 = linear_velocity
	aux.y = -impulse
	return aux


func _on_AnimatedSprite_animation_finished() -> void:
	get_node("AnimatedSprite").play("idle")


func _on_ComboRecorder_down_right_arch() -> void:
	if (!$ShootPerk.is_bullet_out()):
		doShoot()
		

func _on_ComboRecorder_up_full_right_arch() -> void:
	if (!$ShootPerk.is_bullet_out()):
		doShoot(1)
