extends KinematicBody2D
class_name Actor


const FLOOR_NORMAL: Vector2 = Vector2.UP
export var speed: = Vector2(600.0, 1500.0)
export var gravity: float = 3000.0

var _velocity: Vector2 = Vector2.ZERO
