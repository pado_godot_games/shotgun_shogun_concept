extends ComboRecorder

signal up_full_right_arch

const gallainComboDictionary: Dictionary = {
	# [D, R, D+r, R+r]: ["down_right_arch", "↪"],
	# [D, L, D+r, L+r]: ["down_left_arch", "↩"],
	# [U, R, U+r, R+r]: ["up_right_arch", "↷"],
	# [U, L, U+r, L+r]: ["up_left_arch", "↶"],
	[U, UR, L, D, L+r, R, D+r, R+r]: ["up_full_right_arch", "↑↺"],
	# [R, D, R+r, L, D+r, L+r]: ["full_left_arch", "↻"],
}

func _process(delta: float) -> void:
	for key in gallainComboDictionary:
		if keystrokes.slice(keystrokes.size() - key.size(), -1) == key:
			emit_signal(gallainComboDictionary[key][0])
			print(gallainComboDictionary[key][1])
			keystrokes.clear()
