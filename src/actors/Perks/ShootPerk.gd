extends Node


	

export var bullet_initial_distance_from_player: float = 50.0


export(Array, String, FILE, ".tscn") var bullet_scene_paths: Array = ["res://src/objects/bullets/RedBullet.tscn"]
onready var bullet_scenes: Array = []
onready var bullet: Bullet = null


func _ready() -> void:
	for path in bullet_scene_paths:
		bullet_scenes.append(load(path))

# param:
#	direction: true -> forward
#			   false -> backward
# return: 
#	true if a new bullet is shot
# 	false if "the old bullet expolode old bullet" was triggered
func shot(direction: bool, bullet_index: int = 0) -> bool:
	if not bullet:
		bullet = bullet_scenes[bullet_index].instance()
		bullet.name = "bullet"
		bullet.set_bullet_params(direction)
		var player_position: Vector2 = get_parent().position;
		bullet.position = Vector2(bullet_initial_position_x(player_position.x, direction), player_position.y)
		get_tree().current_scene.add_child(bullet)
		return true
	else:
		bullet._destroy_bullet()
		return false
		
func explode_bullet() -> bool:
	if bullet:
		bullet._destroy_bullet()
		return true
	return false

func bullet_initial_position_x(player_pos_x: float, direction: bool) -> float:
	var _sign: float = 1.0 if direction else -1.0
	return player_pos_x + (_sign * bullet_initial_distance_from_player)

func is_bullet_out() -> bool:
	return bullet != null
