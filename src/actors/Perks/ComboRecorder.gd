extends Node

class_name ComboRecorder


const U: String = "action_up"
const D: String = "action_down"
const L: String = "action_left"
const R: String = "action_right"
const actions_to_scan: Array = [U, D, L, R]
const r: String = "_released";

const UR: String = U+r;
const DR: String = D+r;
const LR: String = L+r;
const RR: String = R+r;

var keystrokes: Array = []

signal down_right_arch
signal down_left_arch
signal up_right_arch
signal up_left_arch
signal full_right_arch
signal full_left_arch

const comboDictionary: Dictionary = {
	[D, R, D+r, R+r]: ["down_right_arch", "↪"],
	[D, L, D+r, L+r]: ["down_left_arch", "↩"],
	[U, R, U+r, R+r]: ["up_right_arch", "↷"],
	[U, L, U+r, L+r]: ["up_left_arch", "↶"],
	[L, D, L+r, R, D+r, R+r]: ["full_right_arch", "↺"],
	[R, D, R+r, L, D+r, L+r]: ["full_left_arch", "↻"],
}


var timer
export var combo_reset_time: float = 0.5
func _init():
	timer = Timer.new()
	add_child(timer)
	# timer.autostart = true
	timer.wait_time = combo_reset_time
	timer.connect("timeout", self, "_timeout")

func _input(event: InputEvent) -> void:
	for action in actions_to_scan:
		if event.is_action_pressed(action):
			timer.start()
			keystrokes.append(action)
		if event.is_action_released(action):
			timer.start()
			keystrokes.append(action + r)




func _process(delta: float) -> void:
	for key in comboDictionary:
		if keystrokes.slice(keystrokes.size() - key.size(), -1) == key:
			emit_signal(comboDictionary[key][0])
			print(comboDictionary[key][1])
			keystrokes.clear()

func _timeout() -> void:
	keystrokes.clear()
	timer.stop()

